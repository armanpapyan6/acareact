import  React from 'react'
import Button from "./components/Button/Button";
class App extends React.Component {
    state = { value: 0 };
    onIncrement = () => {
        this.setState({ value: this.state.value + 1 });
    };
    onDecrement = () => {
        this.setState({ value: this.state.value - 1 });
    };
    renderText = () => {
        const { value } = this.state;
        // if (value > 5) {
        //     return <span>value is greater than 5</span>;
        // }
        // if (value < 5) {
        //     return <span>value is less than 5</span>;
        // }
        switch (true){
            case  value >5 :return <span>value is less than 5</span>
            case  value < 5:return <span>value is greater than 5</span>
            default: return <span>value is 5</span>;
        }
        // return
    };
    render() {
        return (
            <div className="App">
                {this.renderText()}
                <Button onClick={this.onIncrement}>increment</Button>
                <Button onClick={this.onDecrement}>decrement</Button>
            </div>
        );
    }
}
export default App